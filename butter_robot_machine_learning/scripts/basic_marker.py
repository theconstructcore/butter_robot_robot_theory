#!/usr/bin/env python

import rospy
from visualization_msgs.msg import Marker, MarkerArray
from geometry_msgs.msg import Point
import random

class MarkerArrayBasics(object):

    def __init__(self, array_positions):
        self.marker_objectlisher = rospy.Publisher('/marker_array_basic', MarkerArray, queue_size=1)
        self.rate = rospy.Rate(1)
        self._array_positions = array_positions
        self.create_markerarray(self._array_positions)
    
    def create_markerarray(self, array_positions):
        self.maker_array = MarkerArray()
        i = 0
        for position in array_positions:
            self.maker_array.markers.append(self.create_marker(i, position))
            i += 1

    def create_marker(self,index, position, scale=0.2, frame_data="base_link"):
        marker_object = Marker()
        marker_object.header.frame_id = frame_data
        marker_object.header.stamp    = rospy.get_rostime()
        marker_object.ns = "butterrobot"
        marker_object.id = index
        marker_object.type = Marker.SPHERE
        marker_object.action = Marker.ADD
        
        my_point = Point()
        my_point.x = position[0]
        my_point.y = position[1]
        my_point.z = position[2]
        marker_object.pose.position = my_point
        
        marker_object.pose.orientation.x = 0
        marker_object.pose.orientation.y = 0
        marker_object.pose.orientation.z = 0.0
        marker_object.pose.orientation.w = 1.0
        marker_object.scale.x = scale
        marker_object.scale.y = scale
        marker_object.scale.z = scale
    
        marker_object.color.r = random.uniform(0, 1)
        marker_object.color.g = random.uniform(0, 1)
        marker_object.color.b = random.uniform(0, 1)
        # This has to be, otherwise it will be transparent
        marker_object.color.a = 1.0
            
        # If we want it for ever, 0, otherwise seconds before desapearing
        marker_object.lifetime = rospy.Duration(0)
        rospy.loginfo("Created Marker "+str(index)+", in position="+str(position))
        return marker_object
    
    def start(self):
        while not rospy.is_shutdown():
            self.marker_objectlisher.publish(self.maker_array)
            self.rate.sleep()
   

if __name__ == '__main__':
    rospy.init_node('marker_basic_node', anonymous=True)
    array_positions = [[0,0,0],
                        [0,0,1],
                        [0,1,0],
                        [0,1,1]]
    markerbasics_object = MarkerArrayBasics(array_positions)
    try:
        markerbasics_object.start()
    except rospy.ROSInterruptException:
        pass
    
