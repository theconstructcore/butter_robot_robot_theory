# Here are the commands for the demos

# Launch the simulation
roslaunch butter_robot_description main_dynamic_control.launch


# Monitor joint data:
rosrun dynamics_butter_robot_pkg dynamics_monitor.py

rosservice call /gazebo/reset_simulation && \
rosbag record /dynamic_data --duration=30 \
  --output-name=data.bag

mv data.bag dynamics_butter_robot_pkg/bag_files



# Lagrange ecuations
sudo pip3 install sympy

