from __future__ import print_function, division
from sympy import symbols, simplify, trigsimp
from sympy.physics.mechanics import (dynamicsymbols, ReferenceFrame, Point, 
                                     inertia, RigidBody, KanesMethod)
from sympy.physics.vector import init_vprinting, vlatex
init_vprinting(use_latex='mathjax', pretty_print=False)

inertial_frame = ReferenceFrame('I') 
lower_link_frame = ReferenceFrame('L') 
upper_link_frame = ReferenceFrame('U') 

theta_1, theta_2 = dynamicsymbols('theta_1 theta_2')

tau_1, tau_2 = dynamicsymbols('tau_1 tau_2')
l_1, l_2 = symbols('l_1 l_2', positive = True)
r_1, r_2 = symbols('r_1 r_2', positive = True)
m_1, m_2, b, g = symbols('m_1 m_2 b g')
I_1_yy, I_2_yy = symbols('I_{1yy}, I_{2yy}') 

lower_link_frame.orient(inertial_frame, 'Axis', [theta_1, inertial_frame.y])
upper_link_frame.orient(lower_link_frame, 'Axis', [theta_2, lower_link_frame.y])

shoulder = Point('S')
shoulder.set_vel(inertial_frame, 0)

elbow = Point('E')
elbow.set_pos(shoulder, l_1 * lower_link_frame.z)
elbow.v2pt_theory(shoulder, inertial_frame, lower_link_frame)

CM_1 = Point('CM_1')
CM_1.set_pos(shoulder, r_1 * lower_link_frame.z)
CM_1.v2pt_theory(shoulder, inertial_frame, lower_link_frame)

CM_2 = Point('CM_2')
CM_2.set_pos(elbow, r_2 * upper_link_frame.z)
CM_2.v2pt_theory(elbow, inertial_frame, upper_link_frame)

I_1 = inertia(lower_link_frame, 0, I_1_yy, 0)            
BODY_1 = RigidBody('lower_link', CM_1, lower_link_frame, m_1, (I_1, CM_1))

I_2 = inertia(upper_link_frame, 0, I_2_yy, 0) 
BODY_2 = RigidBody('upper_link', CM_2, upper_link_frame, m_2, (I_2, CM_2))

P_1 = m_1 * g * inertial_frame.z
r_1_CM = CM_1.pos_from(shoulder).express(inertial_frame)
BODY_1.potential_energy = r_1_CM.dot(P_1)

P_2 = m_2 * g * inertial_frame.z
r_2_CM = CM_2.pos_from(shoulder).express(inertial_frame).simplify()
BODY_2.potential_energy = r_2_CM.dot(P_2)

omega_1, omega_2 = dynamicsymbols('omega_1, omega_2')

kinematical_differential_equations = [omega_1 - theta_1.diff(),
                                      omega_2 - theta_2.diff()]

coordinates = [theta_1, theta_2]
speeds = [omega_1, omega_2]
specified = [tau_1, tau_2]

kane = KanesMethod(inertial_frame, coordinates, speeds, kinematical_differential_equations)

lower_link_frame.set_ang_vel(inertial_frame, omega_1*inertial_frame.y)
upper_link_frame.set_ang_vel(lower_link_frame, omega_2*inertial_frame.y)

lower_link_grav_force_vector = -m_1 * g * inertial_frame.z
lower_link_grav_force = (CM_1, lower_link_grav_force_vector)
upper_link_grav_force_vector = -m_2 * g * inertial_frame.z
upper_link_grav_force = (CM_2, upper_link_grav_force_vector)

lower_link_torque_vector = (tau_1 - b * omega_1) * inertial_frame.y - (tau_2 - b * omega_2) * inertial_frame.y
lower_link_torque = (lower_link_frame, lower_link_torque_vector)
upper_link_torque_vector = (tau_2 - b * omega_2) * inertial_frame.y
upper_link_torque = (upper_link_frame, upper_link_torque_vector)

loads = [lower_link_grav_force,
         upper_link_grav_force,
         lower_link_torque,
         upper_link_torque]

bodies = [BODY_1, BODY_2]

fr, frstar = kane.kanes_equations(bodies, loads)

mass_matrix = trigsimp(kane.mass_matrix_full)

forcing_vector = trigsimp(kane.forcing_full)

from numpy import array, zeros, eye, asarray, dot, rad2deg
from numpy.linalg import inv

from matplotlib.pyplot import plot, xlabel, ylabel, legend, rcParams, show
rcParams['figure.figsize'] = (14, 8)

from sympy import simplify, Matrix, matrix2numpy
from sympy.physics.vector import init_vprinting, vlatex
init_vprinting(use_latex='mathjax', pretty_print=False)

#######################
# LINEALIZATION

equilibrium_point = zeros(len(coordinates + speeds))
equilibrium_point = array([0, 0, 0, 0], dtype=float)
print(type(equilibrium_point))
print(equilibrium_point[0])


print("equilibrium_point="+str(equilibrium_point))

equilibrium_dict = dict(zip(coordinates + speeds, equilibrium_point))
print("equilibrium_dict="+str(equilibrium_dict))

constants = [l_1,
             r_1,
             m_1,
             I_1_yy,
             l_2,
             r_2,
             m_2,
             I_2_yy,
             b,
             g]

numerical_constants = array([0.5019830000000001,  # upper_arm_link_length [m]
                             0.25099150000000003,  # upper_arm_link_com_length [m]
                             0.005,  # upper_arm_link_mass [kg]
                             0.00010930722894083335,  # upper_arm_link_inertia [kg*m^2]
                             0.452117,  # lower_arm_link_length [m]
                             0.2260585,  # lower_arm_link_com_length
                             0.005,  # lower_arm_link_mass [kg]
                             8.933765904083334e-05,  # lower_arm_link_inertia [kg*m^2]
                             0.01,  # joint damping [N s / m]
                             9.81],  # acceleration due to gravity [m/s^2]
                            ) 

parameter_dict = dict(zip(constants, numerical_constants))
print("parameter_dict="+str(parameter_dict))

linearizer = kane.to_linearizer()

A, B = linearizer.linearize(op_point=[equilibrium_dict, parameter_dict], A_and_B=True)

A = matrix2numpy(A, dtype=float)
B = matrix2numpy(B, dtype=float)

print("A="+str(A))

print("B="+str(B))

#######################
# DESIGN THE CONTROLLER
import numpy as np

n = A.shape[0]
controllability_matrix = []
for i in range(n):
    controllability_matrix.append(np.matrix(A) ** i * np.matrix(B))
controllability_matrix = np.hstack(controllability_matrix)

is_controllable = np.linalg.matrix_rank(controllability_matrix) == n
print("is_controllable="+str(is_controllable))

from scipy.linalg import solve_continuous_are

Q = eye(4)
print("Q="+str(Q))

R = eye(2)
print("R="+str(R))

S = solve_continuous_are(A, B, Q, R)
K = dot(dot(inv(R), B.T),  S)
print("K="+str(K))

def controller(x, t):
    """Returns the output of the controller, i.e. the joint torques, given
    the current state."""
    return -dot(K, x)

################################################
# Numerical Integration of the Continuous System 

from numpy import deg2rad, rad2deg, array, zeros, linspace, save
from scipy.integrate import odeint
from pydy.codegen.ode_function_generators import generate_ode_function

constants = [l_1,
             r_1,
             m_1,
             I_1_yy,
             l_2,
             r_2,
             m_2,
             I_2_yy,
             b,
             g]

right_hand_side = generate_ode_function(forcing_vector, coordinates,
                                        speeds, constants,
                                        mass_matrix=mass_matrix,
                                        specifieds=specified)

x0 = zeros(4)
x0[:2] = deg2rad(2.0)

numerical_constants = array([0.5019830000000001,  # upper_arm_link_length [m]
                             0.25099150000000003,  # upper_arm_link_com_length [m]
                             0.005,  # upper_arm_link_mass [kg]
                             0.00010930722894083335,  # upper_arm_link_inertia [kg*m^2]
                             0.452117,  # lower_arm_link_length [m]
                             0.2260585,  # lower_arm_link_com_length
                             0.005,  # lower_arm_link_mass [kg]
                             8.933765904083334e-05,  # lower_arm_link_inertia [kg*m^2]
                             0.01,  # joint damping [N s / m]
                             9.81],  # acceleration due to gravity [m/s^2]
                            )  

frames_per_sec = 60
final_time = 30

t = linspace(0.0, final_time, final_time * frames_per_sec)

y = odeint(right_hand_side, x0, t, args=(controller, numerical_constants))

plot(t, rad2deg(y[:, :2]))
xlabel('Time [s]')
ylabel('Angle [deg]')
legend(["${}$".format(vlatex(c)) for c in coordinates])
show()

