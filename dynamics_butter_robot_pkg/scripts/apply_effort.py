#!/usr/bin/env python

import rospy
from gazebo_msgs.srv import ApplyJointEffort

try:
    apply_joint_effort_srv = rospy.ServiceProxy("/gazebo/apply_joint_effort", ApplyJointEffort)
    apply_joint_effort_srv(joint_name='arm_upper_right_joint', effort=0.01, duration=rospy.Duration(10.0))
except rospy.ServiceException as e:
    print("/gazebo/apply_joint_effort call failed: "+str(e))