from numpy import deg2rad, rad2deg, array, zeros, linspace, load
from sympy.physics.mechanics import dynamicsymbols 
from sympy.physics.vector import vlatex
from matplotlib.pyplot import plot, legend, xlabel, ylabel, rcParams, show
rcParams['figure.figsize'] = (14.0, 6.0)


t = load('t.npy')
y = load('y.npy')
theta_1, theta_2 = dynamicsymbols('theta_1 theta_2')
omega_1, omega_2 = dynamicsymbols('omega_1, omega_2')
coordinates = [theta_1, theta_2]
speeds = [omega_1, omega_2]

plot(t, rad2deg(y[:, :2]))
xlabel('Time [s]')
ylabel('Angle [deg]')
legend(["${}$".format(vlatex(c)) for c in coordinates])
show()

(rad2deg(y[-1,0]), rad2deg(y[-1,0]+y[-1,1]))

plot(t, rad2deg(y[:, 2:]))
xlabel('Time [s]')
ylabel('Angular Rate [deg/s]')
legend(["${}$".format(vlatex(s)) for s in speeds])
show()