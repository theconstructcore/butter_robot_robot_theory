#!/usr/bin/env python

# Import necessary methods for symbolic 
from __future__ import print_function, division
from sympy import symbols, simplify, trigsimp
from sympy.physics.mechanics import (dynamicsymbols, ReferenceFrame, Point, 
                                     inertia, RigidBody, Lagrangian, LagrangesMethod)
from sympy.physics.vector import init_vprinting, vlatex
init_vprinting(use_latex='mathjax', pretty_print=False)

# We reate one ref frame for each ref frame of the system
head_frame = ReferenceFrame('I') 
upper_arm_frame = ReferenceFrame('L') 
lower_arm_frame = ReferenceFrame('U') 

# We supos etht ony we move in the Y axis, in the XZ plane
# We define vars that orient each refframe from each other ( angle and angular speed)
theta_1, theta_2 = dynamicsymbols('theta_1 theta_2')
dtheta_1, dtheta_2 = dynamicsymbols('theta_1 theta_2', 1)

# We define the rest of the variables that define the system
# (𝜏1,𝜏2):are the torques at the joints
# (𝑙1,𝑙2):are the lengths of the links
# (𝑟1,𝑟2): are the distances to the center of mass
# (𝑚1,𝑚2):are the masses of the links
# (𝐼1𝑦𝑦,𝐼2𝑦𝑦):are the moments of inertia (only the component about the Y-axis is needed)
# 𝑏: is the viscous damping parameter of the joints
# 𝑔:: is the gravity acceleration

tau_1, tau_2 = dynamicsymbols('tau_1 tau_2')
l_1, l_2 = symbols('l_1 l_2', positive = True)
r_1, r_2 = symbols('r_1 r_2', positive = True)
m_1, m_2, b, g = symbols('m_1 m_2 b g')
I_1_yy, I_2_yy = symbols('I_{1yy}, I_{2yy}') 


# Set the orientation of the refernce frames
upper_arm_frame.orient(head_frame, 'Axis', [theta_1, head_frame.y])
lower_arm_frame.orient(upper_arm_frame, 'Axis', [theta_2, upper_arm_frame.y])

shoulder = Point('S')
# TODO: Add change of speed, WE SUPOSE THAT THE SPEED OF THE HEAD IS 0
shoulder.set_vel(head_frame, 0)

elbow = Point('E')
elbow.set_pos(shoulder, l_1 * upper_arm_frame.z)
elbow.v2pt_theory(shoulder, head_frame, upper_arm_frame)

CM_1 = Point('CM_1')
CM_1.set_pos(shoulder, r_1 * upper_arm_frame.z)
CM_1.v2pt_theory(shoulder, head_frame, upper_arm_frame)

CM_2 = Point('CM_2')
CM_2.set_pos(elbow, r_2 * lower_arm_frame.z)
CM_2.v2pt_theory(elbow, head_frame, lower_arm_frame)

I_1 = inertia(upper_arm_frame, 0, I_1_yy, 0)            
BODY_1 = RigidBody('upper_arm_link', CM_1, upper_arm_frame, m_1, (I_1, CM_1))

I_2 = inertia(lower_arm_frame, 0, I_2_yy, 0) 
BODY_2 = RigidBody('lower_arm_link', CM_2, lower_arm_frame, m_2, (I_2, CM_2))

# Potential energy

P_1 = m_1 * g * head_frame.z
r_1_CM = CM_1.pos_from(shoulder).express(head_frame)
BODY_1.potential_energy = r_1_CM.dot(P_1)

P_2 = m_2 * g * head_frame.z
r_2_CM = CM_2.pos_from(shoulder).express(head_frame).simplify()
BODY_2.potential_energy = r_2_CM.dot(P_2)

L = Lagrangian(head_frame, BODY_1, BODY_2)

FL = [(upper_arm_frame, (tau_1 - b * dtheta_1) * head_frame.y - (tau_2 - b * dtheta_2) * head_frame.y),
      (lower_arm_frame, (tau_2 - b * dtheta_2) * head_frame.y)]

LM = LagrangesMethod(L, [theta_1, theta_2], frame=head_frame, forcelist=FL)
L_eq = LM.form_lagranges_equations()

mass_matrix = trigsimp(LM.mass_matrix_full)

forcing_vector = trigsimp(LM.forcing_full)

from numpy import deg2rad, rad2deg, array, zeros, linspace, save
from scipy.integrate import odeint
from pydy.codegen.ode_function_generators import generate_ode_function

constants = [l_1,
             r_1,
             m_1,
             I_1_yy,
             l_2,
             r_2,
             m_2,
             I_2_yy,
             b,
             g]

coordinates = [theta_1, theta_2]
speeds = [dtheta_1, dtheta_2]
specified = [tau_1, tau_2]

right_hand_side = generate_ode_function(forcing_vector, coordinates,
                                        speeds, constants,
                                        mass_matrix=mass_matrix,
                                        specifieds=specified)

x0 = zeros(4)
x0[0] = deg2rad(0)
x0[1] = 0.0

numerical_constants = array([0.5019830000000001,  # upper_arm_link_length [m]
                             0.25099150000000003,  # upper_arm_link_com_length [m]
                             0.005,  # upper_arm_link_mass [kg]
                             0.00010930722894083335,  # upper_arm_link_inertia [kg*m^2]
                             0.452117,  # lower_arm_link_length [m]
                             0.2260585,  # lower_arm_link_com_length
                             0.005,  # lower_arm_link_mass [kg]
                             8.933765904083334e-05,  # lower_arm_link_inertia [kg*m^2]
                             0.001,  # joint damping [N s / m]
                             9.81],  # acceleration due to gravity [m/s^2]
                            ) 

numerical_specified = zeros(2)
numerical_specified[0] = 0.01

args = {'constants': numerical_constants,
        'specified': numerical_specified}

frames_per_sec = 60
final_time = 30

t = linspace(0.0, final_time, final_time * frames_per_sec)

y = odeint(right_hand_side, x0, t, args=(numerical_specified, numerical_constants))

save('t.npy', t)
save('y.npy', y)