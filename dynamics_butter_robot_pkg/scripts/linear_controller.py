#!/usr/bin/env python

from __future__ import print_function
import math, rospy

from std_msgs.msg import Float64
from sensor_msgs.msg import JointState
from gazebo_msgs.srv import SetModelConfiguration

def callback(msg):
    print()
    index_arm_lower_left_joint = msg.name.index("arm_lower_left_joint")
    index_arm_upper_left_joint = msg.name.index("arm_upper_left_joint")
    index_arm_lower_right_joint = msg.name.index("arm_lower_right_joint")
    index_arm_upper_right_joint = msg.name.index("arm_upper_right_joint")

    global j1_effort
    global j2_effort
    global j1_r_effort
    global j2_r_effort

    th1 = msg.position[index_arm_upper_left_joint]
    th1 = math.atan2(math.sin(th1), math.cos(th1))
    th2 = msg.position[index_arm_lower_left_joint]
    th2 = math.atan2(math.sin(th2), math.cos(th2))

    th1r = msg.position[index_arm_upper_right_joint]
    th1r = math.atan2(math.sin(th1r), math.cos(th1r))
    th2r = msg.position[index_arm_lower_right_joint]
    th2r = math.atan2(math.sin(th2r), math.cos(th2r))

    dth1 = msg.velocity[index_arm_upper_left_joint]
    dth2 = msg.velocity[index_arm_lower_left_joint]
    dth1r = msg.velocity[index_arm_upper_right_joint]
    dth2r = msg.velocity[index_arm_lower_right_joint]


    factor_d = 1.0

    tau_1 = -(1.04923529e+00*dth1 + 1.14169455e-02*dth2 + 9.93373887e-01*th1 + 9.59274261e-04*th2)*factor_d
    tau_2 = -( 1.14143730e-02*dth1 + 1.01121106e+00*dth2 +  9.56772521e-04*th1 + 9.90408587e-01*th2)*factor_d
    tau_1r = -(1.04923529e+00*dth1r + 1.14169455e-02*dth2r + 9.93373887e-01*th1r + 9.59274261e-04*th2r)*factor_d
    tau_2r = -( 1.14143730e-02*dth1r + 1.01121106e+00*dth2r +  9.56772521e-04*th1r + 9.90408587e-01*th2r)*factor_d
    print("tau_1=>"+str(tau_1))
    print("tau_2=>"+str(tau_2))
    print("tau_1r=>"+str(tau_1r))
    print("tau_2r=>"+str(tau_2r))


    j1_effort.publish(Float64(tau_1))
    j2_effort.publish(Float64(tau_2))
    j1_r_effort.publish(Float64(tau_1r))
    j2_r_effort.publish(Float64(tau_2r))

if __name__ == '__main__':
    # /arm_lower_right_joint_effort_controller/command
    # /arm_upper_right_joint_effort_controller/command
    rospy.init_node('linear_controller', anonymous=True)
    j1_effort = rospy.Publisher('/arm_upper_left_joint_effort_controller/command', 
                                Float64, queue_size=10)
    j2_effort = rospy.Publisher('/arm_lower_left_joint_effort_controller/command', 
                                Float64, queue_size=10)
    j1_r_effort = rospy.Publisher('/arm_upper_right_joint_effort_controller/command', 
                                Float64, queue_size=10)
    j2_r_effort = rospy.Publisher('/arm_lower_right_joint_effort_controller/command', 
                                Float64, queue_size=10)
    rospy.wait_for_service('/gazebo/set_model_configuration')
    rospy.sleep(1.0)
    test_effort = 0.1
    j1_effort.publish(Float64(test_effort))
    j2_effort.publish(Float64(test_effort))
    j1_r_effort.publish(Float64(test_effort))
    j2_r_effort.publish(Float64(test_effort))

    set_joints = rospy.ServiceProxy('/gazebo/set_model_configuration', 
                                    SetModelConfiguration)
    set_joints('butter_robot', 'robot_description', 
               ['arm_upper_left_joint', 'arm_lower_left_joint', 'arm_upper_right_joint', 'arm_lower_right_joint'], 
               [math.radians(0), math.radians(0),math.radians(0), math.radians(0)])
    rospy.Subscriber('/joint_states', JointState, callback)
    rospy.spin()

# K=[[37.58653426  9.12127326 13.03288082  3.7180847 ]
#  [ 7.39629086  8.63609993  3.113795    1.75966805]

# K==[[1.04923529e+00 1.14169455e-02 9.93373887e-01 9.59274261e-04]
#  [1.14143730e-02 1.01121106e+00 9.56772521e-04 9.90408587e-01]]

