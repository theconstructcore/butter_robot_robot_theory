
scale_XYZ = 0.01
arm_upperMass = 0.005
arm_upper_Z_Size = 50.1983
arm_upper_X_Size = 10.1737


arm_lowerMass = 0.005
arm_lower_Z_Size = 45.2117
arm_lower_X_Size = 10.0003

I_upper_arm = arm_upperMass / 12.0 * (arm_upper_Z_Size*scale_XYZ*arm_upper_Z_Size*scale_XYZ + arm_upper_X_Size*scale_XYZ*arm_upper_X_Size*scale_XYZ)
I_lower_arm = arm_lowerMass / 12.0 * (arm_lower_Z_Size*scale_XYZ*arm_lower_Z_Size*scale_XYZ + arm_lower_X_Size*scale_XYZ*arm_lower_X_Size*scale_XYZ)


print("I_upper_arm="+str(I_upper_arm))
print("I_lower_arm="+str(I_lower_arm))