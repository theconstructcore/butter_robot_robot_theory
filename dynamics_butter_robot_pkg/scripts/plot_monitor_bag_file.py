#!/usr/bin/env python

import matplotlib.pyplot as plt
import numpy as np
import rosbag
import rospy
import os

rospy.init_node('dynamics_monitor_plot', anonymous=True)


# get the file path for rospy_tutorials
#base_path = rospack.get_path('dynamics_butter_robot_pkg')
#bag_file_path = os.path.join(base_path, "bag_files/data.bag")
bag_file_path = '/home/tgrip/TheConstruct/ros_playgound/butter_robot_ws/src/butter_robot_robot_theory/dynamics_butter_robot_pkg/bag_files/data.bag'

bag = rosbag.Bag(bag_file_path)
t, data = [], []
for topic, msg, tm in bag.read_messages(topics=['/dynamic_data']):
    t.append(tm.secs+tm.nsecs/1000000000.0)
    data.append(msg.data)
bag.close()
m = np.array(data)

x2, y2, x3, y3 = m[:,0], m[:,1], m[:,6], m[:,7]
plt.plot(x2,y2,x3,y3); plt.axis('equal')
plt.legend(['$CM_2$','$CM_3$'])
plt.title('Trajectory of the center of mass')
plt.show()

th2, th3 = m[:,2], m[:,8]
plt.plot(t,th2,t,th3)
plt.legend(['$th_2$','$th_3$'])
plt.title('Orientation of the link')
plt.show()

vx2, vy2, vx3, vy3 = m[:,3], m[:,4], m[:,9], m[:,10]
plt.plot(t,vx2,t,vy2,t,vx3,t,vy3)
plt.legend(['$v_{x2}$','$v_{y2}$','$v_{x3}$','$v_{y3}$'])
plt.title('Linear velocity of the center of mass')
plt.show()

w2, w3 = m[:,5], m[:,11]
plt.plot(t,w2,t,w3)
plt.legend(['$w_2$','$w_3$'])
plt.title('Angular velocity of the center of mass')
plt.show()

pe, ke = m[:,12], m[:,13]
pe_ref = pe[-1]
plt.plot(t,pe-pe_ref,t,ke)
plt.legend(['potential energy','kinetic energy'])
plt.show()

rospy.loginfo("Finished")

