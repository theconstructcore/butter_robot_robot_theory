#!/usr/bin/env python

import rospy
from sensor_msgs.msg import Image
from geometry_msgs.msg import Point
from cv_bridge import CvBridge, CvBridgeError
import cv2
import numpy as np
import rospkg
import os



class LoadImage(object):

    def __init__(self):
    
        rospy.loginfo("Starting Load Image")
        r = rospkg.RosPack()
        opencv_examples_path = r.get_path('opencv_examples')
        path_to_haar = os.path.join(opencv_examples_path,"haar_cascades")
        self.path_frontal_haar = os.path.join(path_to_haar,"frontalface.xml")
        self.path_eye_haar = os.path.join(path_to_haar,"eye.xml")
        rospy.loginfo("self.path_frontal_haar="+str(self.path_frontal_haar))
        rospy.loginfo("self.path_eye_haar="+str(self.path_eye_haar))

        self.cam_topic = "/cv_camera/image_raw"
        self.check_if_cam_ready()
        self.image_sub = rospy.Subscriber(self.cam_topic, Image, self.camera_callback)
        self.bridge_object = CvBridge()


        # We add a publisher of the face data, Point (x,y,z) are (x,y,area) of the face recognition
        self.face_point_tc = "/face_point"
        self.head_cmd_pub = rospy.Publisher(self.face_point_tc, Point, queue_size=1)

        rospy.loginfo("Starting Load Image...DONE")
    
    def check_if_cam_ready(self):
        cam_image_msg = None
        rospy.logdebug("Waiting for "+self.cam_topic+" to be READY...")
        while cam_image_msg is None and not rospy.is_shutdown():
            try:
                cam_image_msg = rospy.wait_for_message(self.cam_topic, Image, timeout=5.0)
                rospy.logdebug("Current "+self.cam_topic+" READY=>")

            except Exception as ex:
                print(ex)
                rospy.logerr("Current "+self.cam_topic+" not ready yet, retrying for getting cam_image_msg")

        rospy.logdebug("DONE,"+self.cam_topic+" is READY...")

    def camera_callback(self,data):
        try:
            # We select bgr8 because its the OpenCV encoding by default
            cv_image = self.bridge_object.imgmsg_to_cv2(data, desired_encoding="bgr8")
        except CvBridgeError as e:
            print(e)


        face_cascade = cv2.CascadeClassifier(self.path_frontal_haar)
        eyes_cascade = cv2.CascadeClassifier(self.path_eye_haar)

        
        img = cv_image
        #img = cv2.resize(img,(350,550))
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        faces = face_cascade.detectMultiScale(gray, 1.3, 5)
        

        for (x,y,w,h) in faces:
            cv2.rectangle(img,(x,y),(x+w,y+h),(255,0,0),2)
            roi_gray = gray[y:y+h, x:x+w]
            roi_color = img[y:y+h, x:x+w]
            area = w * h
            msg_point = Point()
            msg_point.x = x
            msg_point.y = y
            msg_point.z = area
            self.head_cmd_pub.publish(msg_point)
            
            eyes = eyes_cascade.detectMultiScale(roi_gray)
            for (ex,ey,ew,eh) in eyes:
                cv2.rectangle(roi_color, (ex,ey),(ex+ew,ey+eh),(0,255,0),2)
           
        cv2.imshow('image',img)
        

        cv2.waitKey(1)



def main():
    
    rospy.init_node('load_image_node', anonymous=True)
    load_image_object = LoadImage()
    try:
        rospy.spin()
    except KeyboardInterrupt:
        print("Shutting down")
    cv2.destroyAllWindows()

if __name__ == '__main__':
    main()
