#!/usr/bin/env python

import rospy
from trajectory_msgs.msg import JointTrajectory, JointTrajectoryPoint
import time

class MoveButterRobot(object):

    def __init__(self):

        self.head_cmd = "/lower_head_controller/command"
        self.head_cmd_pub = rospy.Publisher(self.head_cmd, JointTrajectory, queue_size=1)
        self.check_publishers_connection()
    
    def check_publishers_connection(self):
        """
        Checks that all the publishers are working
        :return:
        """
        rate = rospy.Rate(10)  # 10hz
        while (self.head_cmd_pub.get_num_connections() == 0 and not rospy.is_shutdown()):
            rospy.logdebug("No susbribers to head_cmd_pub yet so we wait and try again")
            try:
                rate.sleep()
            except rospy.ROSInterruptException:
                # This is to avoid error when world is rested, time when backwards.
                pass
        rospy.logdebug("head_cmd_pub Publisher Connected")

        rospy.logdebug("All Publishers READY")
    
    def move_head(self, head_angle, sec_duration=1.0):
        msg = JointTrajectory()
        """
        rosmsg show trajectory_msgs/JointTrajectory
        std_msgs/Header header
        uint32 seq
        time stamp
        string frame_id
        string[] joint_names
        trajectory_msgs/JointTrajectoryPoint[] points
        float64[] positions
        float64[] velocities
        float64[] accelerations
        float64[] effort
        duration time_from_start

        Example:

        header: 
            seq: 10917
            stamp: 
                secs: 0
                nsecs:         0
            frame_id: ''
        joint_names: 
        - lower_head_joint
        points: 
        - 
            positions: [0.5]
            velocities: []
            accelerations: []
            effort: []
            time_from_start: 
            secs: 1
            nsecs:         0
        """
        msg.header.stamp = rospy.Time.now()
        msg.header.frame_id = ""
        msg.joint_names = ["lower_head_joint"]

        point_obj = JointTrajectoryPoint()
        point_obj.positions = [head_angle]
        # Duration in seconds
        point_obj.time_from_start = rospy.Duration.from_sec(sec_duration)
        msg.points = [point_obj]

        rospy.logdebug("Publishing message...")
        self.head_cmd_pub.publish(msg)
        time.sleep(sec_duration)
        rospy.logdebug("Publishing message...DONE")


if __name__ == "__main__":
    rospy.init_node('move_butter_robot', anonymous=True, log_level=rospy.DEBUG)
    mv = MoveButterRobot()
    mv.move_head(0.7, sec_duration=10.0)    
    mv.move_head(-0.7)
