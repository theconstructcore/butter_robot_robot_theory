#!/usr/bin/env python

import rospy
from move_butter_robot import MoveButterRobot
from geometry_msgs.msg import Point

class HeadFaceTracker(object):

    def __init__(self):

        self.ERROR_Y = 20
        self.CENTER_Y = 240

        self.mv = MoveButterRobot()
        self.head_angle = 0.0
        self.head_delta = 0.01
        self.mv.move_head(self.head_angle, sec_duration=1.0)   

        self.face_point_tc = "/face_point"
        self.check_if_face_detect_ready()
        self.image_sub = rospy.Subscriber(self.face_point_tc, Point, self.face_callback)
    
    def check_if_face_detect_ready(self):
        face_image_msg = None
        rospy.logdebug("Waiting for "+self.face_point_tc+" to be READY...")
        while face_image_msg is None and not rospy.is_shutdown():
            try:
                face_image_msg = rospy.wait_for_message(self.face_point_tc, Point, timeout=5.0)
                self.update_delta_y(face_image_msg)
                rospy.logdebug("Current "+self.face_point_tc+" READY=>")

            except Exception as ex:
                print(ex)
                rospy.logerr("Current "+self.face_point_tc+" not ready yet, retrying for getting face_image_msg")

        rospy.logdebug("DONE,"+self.face_point_tc+" is READY...")
    
    def face_callback(self,msg):
        self.update_delta_y(msg)
    
    def update_delta_y(self, msg):
        x_corner = msg.x 
        y_corner = msg.y 
        area = msg.z

        self.delta_y = y_corner - self.CENTER_Y
    
    def loop(self):

        r = rospy.Rate(5)
        while not rospy.is_shutdown():

            if self.delta_y >= self.ERROR_Y:
                # Face is detected up, so we lower the face angle
                self.head_angle += self.head_delta
                rospy.loginfo("LOWER HEAD")
            elif self.delta_y < -1*self.ERROR_Y:
                # Face is detected down, so we lift the face angle
                self.head_angle -= self.head_delta
                rospy.loginfo("LIFT HEAD")
            else:
                # Face is cenetered, we dont move
                rospy.loginfo("DONT MOVE HEAD")

            rospy.logdebug("self.delta_y="+str(self.delta_y))
            rospy.logdebug("head_angle="+str(self.head_angle))

            self.mv.move_head(self.head_angle, sec_duration=0.5)
            r.sleep()



if __name__ == "__main__":
    rospy.init_node('head_follow_face', anonymous=True, log_level=rospy.INFO)
    mv = HeadFaceTracker()
    mv.loop()
