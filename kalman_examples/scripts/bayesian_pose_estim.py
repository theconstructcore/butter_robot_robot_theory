#!/usr/bin/env python

import rospy
from std_msgs.msg import Int32
from sensor_msgs.msg import LaserScan
from std_srvs.srv import Empty, EmptyResponse
import math
# For Rviz visualization
from visualization_msgs.msg import Marker
from geometry_msgs.msg import Quaternion, Pose, Point, Vector3
from std_msgs.msg import Header, ColorRGBA
from discrete_move import DiscreteMove
from kalman_examples.srv import GridMove, GridMoveRequest, GridMoveResponse

class BayesianObj(object):

    def __init__(self):
        ### INIT VARS
        # Discrete movement
        self.dm = DiscreteMove()
        # kernel
        self.init_vars()
        
        # Global, for Rviz Markers
        self.marker_publisher = rospy.Publisher('visualization_marker', Marker, queue_size=10)
        # Subscribe to the ROS topic called "/sensor_data"
        rospy.Subscriber("/top_scan", LaserScan, self.sensor_callback)
        # motion_update service server
        self.motion_update = rospy.Service('/request_motion_update', GridMove, self.motion_srv_callback)
        # measurement_update service server
        self.measurement_update = rospy.Service('/request_measurement_update', Empty, self.measurement_srv_callback)
        # Create a new ROS rate limiting timer
        self.rate = rospy.Rate(5)

        

    
    def init_vars(self):
        # change these values modifying ROS parameters
        self.kernel = [0.1, 0.8, 0.1]
        self.light_sensor_accuracy_rate = 0.9
        ### Add initial belief HERE ### 
        #self.belief = [0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1]
        self.belief = [0,0,1,0,0,0,0,0,0,0]

        ### Add corridor map HERE ### 
        self.corridor_map = [0,1,0,1,0,0,0,1,0,0]
        self.bayes_filter_iteration = 0.0

        # Sensor range
        self.sensor_max_range = 2.0

        # Grid start number
        self.grid_number = 0
        self.dm.move_to_grid(self.grid_number)


    ### Add predict_step function HERE ###
    def predict_step(self, belief, offset, kernel):
        """Applies a convolution by sliding kernel over the belief"""
        N = len(belief)
        kN = len(kernel)
        width = int((kN - 1) / 2)
        output = [0] * N
        for i in range(N):
            for k in range (kN):
                index = (i + (width-k) - offset) % N
                output[i] += belief[index] * kernel[k]
        return output

    # Callback function to handle new sensor data received
    def sensor_callback(self,sensor_data):
        
        self.sensor_max_range = sensor_data.range_max
        self.raw_sensor_reading = sensor_data.ranges[0]    

    def update_sensor_calculations(self):
        if self.raw_sensor_reading == math.inf:
            clean_raw_sensor_reading = self.sensor_max_range
        else:
            clean_raw_sensor_reading = self.raw_sensor_reading
        rospy.loginfo("Light sensor data received: '%f'" % (clean_raw_sensor_reading))
        
        if clean_raw_sensor_reading >= self.sensor_max_range:            
            sensor_reading = 0
        else:
            sensor_reading = 1
        
        rospy.logwarn("Sensor Detected ?="+str(sensor_reading))


        ### ADD BAYES FILTER CORRECT STEP HERE ###
        likelihood_estimation = self.likelihood(self.corridor_map, z=sensor_reading, z_prob=self.light_sensor_accuracy_rate)
        self.belief = self.correct_step(likelihood_estimation, self.belief)
        print(self.belief)
        # Print current belief to console
        self.print_belief()

        ### Visualize in Rviz ###
        self.create_rviz_markers(self.marker_publisher, self.belief)
    

    ### Add likelihood FUNCTION HERE ###
    def likelihood(self,world_model, z, z_prob):
        """ Calculates likelihood that a measurement matches a positions in the map """
        # create output vector
        likelihood = [1] * (len(world_model))
        for index, grid_value in enumerate(world_model):
            print("#################")
            print(str(likelihood))
            print("grid_value="+str(grid_value)+",z="+str(z))
            if grid_value == z:
                likelihood[index] *= z_prob
            else:
                likelihood[index] *= (1 - z_prob)
            print(str(likelihood))
            print("#################")
        return likelihood

    
    ### Add correct_step function HERE ###
    def correct_step(self,likelihood, belief):
        output = []
        print("@@@@@@@@@@@@@@@")
        print("likelihood="+str(likelihood))
        print("belief="+str(belief))
        # element-wise multiplication (likelihood * belief)
        for i in range(0, len(likelihood)):
            print("likelihood[i]="+str(likelihood[i])+",belief[i]="+str(belief[i]))
            output.append(likelihood[i]*float(belief[i]))
            print("output="+str(output))
        print("@@@@@@@@@@@@@@@")
        return self.normalize(output)

    
    ### Add normalize function HERE ###
    def normalize(self,inputList):
        """ calculate the normalizer, using: (1 / (sum of all elements in list)) """
        normalizer = 1 / float(sum(inputList))
        # multiply each item by the normalizer
        inputListNormalized = [x * normalizer for x in inputList]
        return inputListNormalized

    
    def print_belief(self):
        """ Prints current belief to console """
        print("\nProbabilities of the robot presence at every grid cell:")
        for idx, prob_value in enumerate(self.belief):
            print("Grid cell %d probability: %f" % (idx, prob_value))
        print("\n")

        m_values = max(self.belief)
        max_probability_grid_cells = [i for i, j in enumerate(self.belief) if j == m_values]
        print("Current position best estimate %06.4f %%, corresponding to grid cell(s):" % (m_values*100) )
        print(max_probability_grid_cells)
        print("\n")

    ##### RVIZ MARKERS ########
    def create_rviz_markers(self,marker_pub, dist):
        for idx, prob_value in enumerate(dist):
            # print("Grid cell %d probability: %f" % (idx, prob_value))
            text_marker = Marker(
                        type=Marker.TEXT_VIEW_FACING,
                        id=idx,
                        lifetime=rospy.Duration(0),
                        pose=Pose(Point(-4.5+idx, 0.0, 0.15+prob_value*10), Quaternion(0, 0, 0, 1)),
                        scale=Vector3(0.01, 0.01, 0.4),
                        header=Header(frame_id='map'),
                        color=ColorRGBA(0.0, 1.0, 0.0, 0.8),
                        text=str(prob_value)[:5])
            marker_pub.publish(text_marker)
            rospy.sleep(0.02)
            column_marker = Marker(
                        type=Marker.CUBE,
                        id=10+idx,
                        lifetime=rospy.Duration(0),
                        pose=Pose(Point(-4.5+idx, 0.0, (prob_value*10)/2), Quaternion(0, 0, 0, 1)),
                        scale=Vector3(0.2, 0.01, prob_value*10),
                        header=Header(frame_id='map'),
                        color=ColorRGBA(0.0, 1.0, 0.0, 0.8))
            marker_pub.publish(column_marker)
            rospy.sleep(0.02)

    def motion_srv_callback(self,request):
        # Call to Bayes Filter predict step (offset (distance) z = 1)
        # We use the same service message to publish the increments
        offset = request.gridnum
        self.grid_number += offset
        if self.grid_number > 9:
            # We reset
            self.grid_number = 0
        self.dm.move_to_grid(self.grid_number)
        self.belief = self.predict_step(self.belief, offset, self.kernel)
        # Visualize in Rviz
        self.create_rviz_markers(self.marker_publisher, self.belief)
        response = GridMoveResponse()
        response.success = True
        return response


    
    def measurement_srv_callback(self, request_msg):
        self.update_sensor_calculations()
        response = EmptyResponse()
        return response


def main():
    rospy.init_node('bayesian_butterrobot__node', anonymous=True, log_level=rospy.DEBUG)
    bo = BayesianObj()
    
    # Wait for at least 1 subscriber before publishing anything
    while bo.marker_publisher.get_num_connections() == 0 and not rospy.is_shutdown():
      #rospy.logwarn_once("Waiting for a subscriber to the topic...")
      print(bo.marker_publisher.get_num_connections())
      rospy.logwarn("Waiting for a subscriber for: /visualization_marker")
      rospy.sleep(5)

    # Print node start information
    print("-----------------------------------------------------\n")
    print("Started Bayes Filter node")
    print("Waiting for sensor data...")
    print("-----------------------------------------------------\n")
    
    ### Visualize initial belief in Rviz ###
    bo.create_rviz_markers(bo.marker_publisher, bo.belief)
    
    # Execute indefinitely until ROS tells the node to shut down.
    while not rospy.is_shutdown():

        # Sleep appropriately so as to limit the execution rate
        bo.rate.sleep()

if __name__ == '__main__':
    main()
