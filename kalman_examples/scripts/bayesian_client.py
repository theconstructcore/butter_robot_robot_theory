#!/usr/bin/env python

import rospy
from std_srvs.srv import Empty, EmptyRequest
from kalman_examples.srv import GridMove, GridMoveRequest

class BayesianClientObj(object):

    def __init__(self):

        # Wait for the service client /gazebo/delete_model to be running        
        rospy.wait_for_service('/request_measurement_update')
        rospy.wait_for_service('/request_motion_update')

        # Create the connection to the service        
        self.measurement_update_service = rospy.ServiceProxy('/request_measurement_update', Empty)
        self.motion_update_service = rospy.ServiceProxy('/request_motion_update', GridMove)

        self.measurement_update_request = EmptyRequest()
        self.motion_update_request = GridMoveRequest()

        self.motion_update_request.gridnum = 1

    def step(self):
        result_measurement_update = self.measurement_update_service(self.measurement_update_request)
        result_motion_update = self.motion_update_service(self.motion_update_request)
        # Print the result given by the service called
        print("result_measurement_update="+str(result_measurement_update))
        print("result_motion_update="+str(result_motion_update))
    
    def loop(self):

        while not rospy.is_shutdown():
            self.step()

def main():
    rospy.init_node('bayesian_butterrobot_client_node', anonymous=True, log_level=rospy.DEBUG)
    bco = BayesianClientObj()
    bco.loop()

if __name__ == '__main__':
    main()
