#!/usr/bin/env python

import rospy
from geometry_msgs.msg import Twist
from nav_msgs.msg import Odometry
from kalman_examples.srv import GridMove, GridMoveRequest, GridMoveResponse

class DiscreteMove(object):

    def __init__(self):

        # Global, for Rviz Markers
        self.cmd_vel = rospy.Publisher('/cmd_vel', Twist, queue_size=1)
        # Subscribe to the ROS topic called "/movement_data"
        self.odom_topic = "/odom"
        self.pos_x = self._check_odom_ready()
        rospy.Subscriber(self.odom_topic, Odometry, self.odom_callback)

        # Service for movement
        self.motion_update = rospy.Service('/move_to_grid', GridMove, self.motion_srv_callback)

        self.odom_error = 0.05
        self.min_delta_to_slow = 0.5
        self.SLOW_SPEED = 0.1
        self.NORMAL_SPEED = 0.5

        self.r = rospy.Rate(10)

    def motion_srv_callback(self,request):
        num_grid_to_move = request.gridnum
        #rospy.loginfo("Requestedto move to grid=="+str(num_grid_to_move))
        self.move_to_grid(num_grid_to_move)
        response = GridMoveResponse()
        response.success = True
        return response

    def _check_odom_ready(self):
        self.odom = None
        while self.odom is None and not rospy.is_shutdown():
            try:
                self.odom = rospy.wait_for_message(self.odom_topic, Odometry, timeout=1.0)
                #rospy.logdebug("Current "+self.odom_topic+" READY=>" + str(self.odom))

            except:
                pass
                #rospy.logerr("Current "+self.odom_topic+" not ready yet, retrying for getting odom")

        return self.odom.pose.pose.position.x
    
    def odom_callback(self,msg):
        self.pos_x = msg.pose.pose.position.x
    
    def move_to_grid(self,grid_num):
        """
        Moves robot to grid specified
        """       
        rospy.loginfo("Moving to grid num="+str(grid_num))
        while not self.check_position_in_range(grid_num) and not rospy.is_shutdown():
            self.r.sleep()
        rospy.loginfo("DONE Moving to grid num="+str(grid_num))
    
    def check_position_in_range(self, desired_grid):

        cmd_msg = Twist()
        
        finished = False

        delta = desired_grid - self.pos_x

        if abs(delta) >= self.odom_error:
            # It has to move
            # Speed? Is it close?
            linear_speed = self.NORMAL_SPEED

            if abs(delta) <= self.min_delta_to_slow:
                linear_speed = self.SLOW_SPEED
                
            # It has to move +X o -X ?
            if delta < 0:
                # -X
                linear_speed *= -1 
        else:
            #It has to stop
            linear_speed = 0.0
            finished = True
        
        cmd_msg.linear.x = linear_speed
        self.cmd_vel.publish(cmd_msg)

        #rospy.loginfo("delta="+str(delta))
        #rospy.loginfo("linear_speed="+str(linear_speed))
        #rospy.loginfo("finished="+str(finished))

        return finished



def main():
    rospy.init_node('descrete_move_butterrobot_node', anonymous=True, log_level=rospy.DEBUG)
    dm = DiscreteMove()
    rospy.spin()

if __name__ == '__main__':
    main()
